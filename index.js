// Concepts and Theory Exam:
// https://forms.gle/GapYaHb6LWkNHw1Z6 


// Mock Technical Exam (Function Coding)
// No need to create a test inputs, the program automatically checks any input through its parameter/s
// Use the parametrs as your test input
// Make sure to use return keyword so your answer will be read by the tester (Example: return result;);
// - Do not add or change parameters
// - Do not create another file
// - Do not share this mock exam

// Use npm install and npm test to test your program

function countLetter(letter, sentence) 
{
    let result = 0;
    
    for (let position = 0; position < letter.length;position++)
    {
        if (letter.charAt(position) == sentence)
        {
            result +=1;
        }
    }
return result;
}
console.log(countLetter('counting','o'));




    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
        
        text = text.toLowerCase();
        let len = text.length;
  
        let arr = text.split('');
  
        arr.sort();
        for (let i = 0; i < len - 1; i++) {
            if (arr[i] == arr[i + 1])
                return false;
        }
        return true;
    }
     
    let text1 = "Machine";
    if(isIsogram(text1))
    {
        document.write("True");
    }
    else{
        document.write("False");
    }
 
    let text2 = "isogram";
    if(isIsogram(text2))
    {
        document.write("True");
    }
    else{
        document.write("False");
    }
 
    let text3 = "GeeksforGeeks";
    if(isIsogram(text3))
    {
        document.write("True");
    }
    else{
        document.write("False");
    }
 
    let text4 = "Alphabet";
    if(isIsogram(text4))
    {
        document.write("True");
    }
    else{
        document.write("False");
    }
     

//__________________________________________

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
 

    if (age < 13) {
        return undefined;
    }
    else if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = price *0.8 ;
        return discountedPrice.toFixed(2);
    }
    else {
        return price.toFixed(2);
    }
    
} 


//_______________________________________________

// Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories


function findHotCategories(items) {
    
     let ilen = items.length
     for(let i = 0; i < ilen; i++){
       if(items[i].stocks === 0){
         let newArr = items[i].category
            return [newArr]
       }
    }
  };
      
  const items = [
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    ];

const emptyStocks = [];
for( let a in items ){ if( items[a].stocks < 1 ) emptyStocks.push(items[a]); }
console.log( emptyStocks );


//____________________________________________________

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const tempVoter = candidateA.filter((data) => candidateB.includes(data));
    return tempVoter;

}

    console.log(findFlyingVoters(
    candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'],
    candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  ));



    


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};